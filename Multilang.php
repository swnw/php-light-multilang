<?php

namespace swnw;

/*
Hugo Le Baher - swnw
29/05/2017

Multilang - v1.0.0
Module permettant de charger des fichiers de langues dans des fichiers php
*/

class Multilang
{

    private $words = array();
    private $lang = "fr";

    function __construct($files = "lang/", $separator = "=", $extension = "txt") {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        if(isset($_GET["lang"])){
            $this->lang = $_GET["lang"];
            $_SESSION["lang"] = $this->lang;
        }elseif(isset($_SESSION["lang"])){
            $this->lang = $_SESSION["lang"];
        }

        $path = $files."lang_".$this->lang.".".$extension;
        if(file_exists($path)){
            $lines = file($path);
            foreach ($lines as $line){
                $parts = explode($separator, $line);
                if(count($parts)>1){ // Si ligne != vide
                    $this->words[$parts[0]] = $parts[1];
                }
            }
        }else{
            echo "[file not found]";
        }
    }

    function getText( $key ){
        if(isset($this->words[$key])){
            return $this->words[$key];
        }else{
            return "[loading error]";
        }
    }

    function getLanguage(){
        return $this->lang;
    }

}

?>